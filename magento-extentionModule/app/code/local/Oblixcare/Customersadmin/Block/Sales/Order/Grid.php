<?php


class Oblixcare_Customersadmin_Block_Sales_Order_Grid extends Mage_Adminhtml_Block_Sales_Order_Grid
{
    /**
	 * Loading Order collection with marked difference b/w billing and shipping address
	 * 
	 * 
	 * @return collection object 
	 */
    protected function _prepareCollection()
    {
        $collection = Mage::getResourceModel($this->_getCollectionClass());

        $collection = $this->isBillingShippingMatchRow($collection);

        $this->setCollection($collection);
        return parent::_prepareCollection();
    }
    /**
	 * Compare Billing vs Shipping Order Address
	 * 
	 * @param object $collection
	 * @return collection object 
	 */
    protected function isBillingShippingMatchRow($collection)
    {
        $billingAliasName = 'billing_o_a';
        $shippingAliasName = 'shipping_o_a';
        $joinTable = $collection->getTable('sales/order_address');

        $collection->addExpressionFieldToSelect('same_street_address', 'IF({{ billing_street }} = {{ shipping_street }}, "true", "false")',
            array('billing_street' => $billingAliasName . '.street', 'shipping_street' => $shippingAliasName . '.street')
        );


        $collection->getSelect()
            ->joinLeft(array($billingAliasName => $joinTable), "(main_table.entity_id = $billingAliasName.parent_id AND $billingAliasName.address_type = 'billing')", array($billingAliasName . '.street')
            )
            ->joinLeft(
                array($shippingAliasName => $joinTable), "(main_table.entity_id = $shippingAliasName.parent_id AND $shippingAliasName.address_type = 'shipping')", array($shippingAliasName . '.street')
            );

        return $collection;
    }
    /**
	 * Convert a image to gd resource
	 * 
	 * @row object 
	 * @return string message
	 */
    public function getRowClass($row)
    {

        if ($row->getSameStreetAddress() == 'true') {
            return '';
        } else {
            return 'different_street_address';
        }

    }

}